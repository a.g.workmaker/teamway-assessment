# Teamway Skill Assessment
#### Introduction
- This project is based on [psycologies.co.uk](https://www.psychologies.co.uk/self/are-you-an-introvert-or-an-extrovert.html) questions.
- It fetchs 14 questions from a mock backend.
- It shows 3 to 5 question to user randomely.
- I have no Idea how to calculate Extroversion and Introversion, lets think in all questions options have a rank. for example, 1st option: more introvert, 2nd option: public introvert and private extrovert, 3rd option: public extrovert and private introvert, 4th option: more extrovert
- It calculates the score, lable of score, and description


#### Installation
```sh
git@gitlab.com:a.g.workmaker/teamway-assessment.git
cd teamway-assessment
npm install
npx react-native link
npx react-native run-ios
```

